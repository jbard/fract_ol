/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_prog.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/22 16:43:05 by jbard             #+#    #+#             */
/*   Updated: 2018/05/03 17:13:24 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void		ft_last_part(t_mlx *mlx, int i)
{
	while (++i < IMG_SIZE)
	{
		if (LAYER(layer_hud[i]) != -1)
			IMG(data[i]) = LAYER(layer_hud[i]);
		else
			IMG(data[i]) = LAYER(layer_fract[i]);
	}
}

static void		ft_part_hud(t_mlx *mlx, int i)
{
	if (PARAM(right_panel_is_open))
		while (++i < IMG_SIZE)
		{
			if (i % WIN_W > WIN_W - WIN_W_PRE && LAYER(layer_prev[i]) != -1)
				IMG(data[i]) = LAYER(layer_prev[i]);
			else if (LAYER(layer_hud[i]) != -1)
				IMG(data[i]) = LAYER(layer_hud[i]);
			else
				IMG(data[i]) = LAYER(layer_fract[i]);
		}
	else if (PARAM(left_panel_is_open))
		while (++i < IMG_SIZE)
		{
			if (i % WIN_W < PANEL_W_SIZE && LAYER(layer_prev[i]) != -1)
				IMG(data[i]) = LAYER(layer_prev[i]);
			else if (LAYER(layer_hud[i]) != -1)
				IMG(data[i]) = LAYER(layer_hud[i]);
			else
				IMG(data[i]) = LAYER(layer_fract[i]);
		}
	else
		ft_last_part(mlx, i);
}

static void		ft_merge_layer(t_mlx *mlx)
{
	int		i;

	i = -1;
	if (!PARAM(show_panels))
		while (++i < IMG_SIZE)
			IMG(data[i]) = LAYER(layer_fract[i]);
	else if (PARAM(right_panel_is_open) && PARAM(left_panel_is_open))
		while (++i < IMG_SIZE)
		{
			if (LAYER(layer_prev[i]) != -1)
				IMG(data[i]) = LAYER(layer_prev[i]);
			else if (LAYER(layer_hud[i]) != -1)
				IMG(data[i]) = LAYER(layer_hud[i]);
			else
				IMG(data[i]) = LAYER(layer_fract[i]);
		}
	else
		ft_part_hud(mlx, i);
}

void			ft_draw_all(t_mlx *mlx)
{
	mlx_clear_window(mlx->mlx_ptr, mlx->win);
	ft_merge_layer(mlx);
	mlx_put_image_to_window(mlx->mlx_ptr, mlx->win, IMG(img_ptr), 0, 0);
}

void			ft_draw_first(t_mlx *mlx)
{
	ft_main_fractal(mlx);
	if (PARAM(show_panels))
	{
		ft_draw_hud(mlx);
		ft_draw_pre(mlx);
	}
	ft_merge_layer(mlx);
	mlx_put_image_to_window(mlx->mlx_ptr, mlx->win, IMG(img_ptr), 0, 0);
}
