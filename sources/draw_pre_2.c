/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_pre_2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 21:02:09 by jbard             #+#    #+#             */
/*   Updated: 2018/05/04 14:51:18 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_pre_effect_param(t_mlx *mlx, int x, int y, int effect)
{
	int		param[7];
	int		*tab_ite;

	if (PARAM(fractal) == 0)
		tab_ite = FRACT(preview_man_ite);
	else if (PARAM(fractal) == 1)
		tab_ite = FRACT(preview_ju_ite);
	else if (PARAM(fractal) == 2)
		tab_ite = FRACT(preview_burn_ite);
	else
		tab_ite = FRACT(preview_mult_ite);
	param[0] = x;
	param[1] = y;
	param[2] = WIN_W_PRE;
	param[3] = WIN_H_PRE;
	param[4] = WIN_W;
	param[5] = PARAM(color);
	param[6] = 50;
	ft_effect(mlx, effect, tab_ite, param);
}

void		ft_effect_menu(t_mlx *mlx)
{
	LAYER(layer_tmp) = LAYER(layer_prev);
	ft_pre_effect_param(mlx, 5, 5, 0);
	ft_pre_effect_param(mlx, 5, PANEL_H_SIZE + 5, 1);
	ft_pre_effect_param(mlx, 5, PANEL_H_SIZE * 2 + 5, 2);
	ft_pre_effect_param(mlx, 5, PANEL_H_SIZE * 3 + 5, 3);
	ft_pre_effect_param(mlx, 5, PANEL_H_SIZE * 4 + 5, 4);
}

void		ft_left_pre(t_mlx *mlx)
{
	ft_effect_menu(mlx);
}
