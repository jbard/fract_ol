/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_event_hud.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 20:39:11 by jbard             #+#    #+#             */
/*   Updated: 2018/05/02 15:25:56 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_event_right_panel(t_mlx *mlx, int tmp)
{
	if (PARAM(x) < WIN_W - tmp)
		PARAM(right_handle_color) = MENU_DRAWER;
	else
		PARAM(right_handle_color) = MENU_HANDLE;
	ft_draw_hud(mlx);
	if (PARAM(x) > WIN_W - tmp)
	{
		if (PARAM(y) < PANEL_H_SIZE)
			ft_draw_high(mlx, WIN_W - WIN_W / 6, 0, MENU_PREVIEW_H);
		else if (PARAM(y) < PANEL_H_SIZE * 2)
			ft_draw_high(mlx, WIN_W - WIN_W / 6, PANEL_H_SIZE, MENU_PREVIEW_H);
		else if (PARAM(y) < PANEL_H_SIZE * 3)
			ft_draw_high(mlx, WIN_W - WIN_W / 6,
					PANEL_H_SIZE * 2, MENU_PREVIEW_H);
		else if (PARAM(y) < PANEL_H_SIZE * 4)
			ft_draw_high(mlx, WIN_W - WIN_W / 6,
					PANEL_H_SIZE * 3, MENU_PREVIEW_H);
	}
}

static void	ft_effect_event(t_mlx *mlx)
{
	if (PARAM(y) < PANEL_H_SIZE)
		ft_draw_high(mlx, 0, 0, MENU_PREVIEW_H);
	else if (PARAM(y) < PANEL_H_SIZE * 2)
		ft_draw_high(mlx, 0, PANEL_H_SIZE, MENU_PREVIEW_H);
	else if (PARAM(y) < PANEL_H_SIZE * 3)
		ft_draw_high(mlx, 0, PANEL_H_SIZE * 2, MENU_PREVIEW_H);
	else if (PARAM(y) < PANEL_H_SIZE * 4)
		ft_draw_high(mlx, 0, PANEL_H_SIZE * 3, MENU_PREVIEW_H);
	else
		ft_draw_high(mlx, 0, PANEL_H_SIZE * 4, MENU_PREVIEW_H);
}

static void	ft_event_left_panel(t_mlx *mlx, int tmp)
{
	if (PARAM(x) > tmp)
		PARAM(left_handle_color) = MENU_DRAWER;
	else
		PARAM(left_handle_color) = MENU_HANDLE;
	ft_draw_hud(mlx);
	if (PARAM(x) < tmp)
		ft_effect_event(mlx);
}

void		ft_draw_event_hud(t_mlx *mlx)
{
	int		right_tmp;
	int		left_tmp;

	right_tmp = (PANEL_W_SIZE * PARAM(right_panel_is_open));
	left_tmp = (PANEL_W_SIZE * PARAM(left_panel_is_open));
	if (PARAM(x) > WIN_W - right_tmp - 10)
		ft_event_right_panel(mlx, right_tmp);
	else if (PARAM(x) < left_tmp + 11)
		ft_event_left_panel(mlx, left_tmp);
	PARAM(clean_high) = 1;
	PARAM(draw) = 1;
}
