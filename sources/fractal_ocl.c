/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractal_ocl.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 16:26:24 by jbard             #+#    #+#             */
/*   Updated: 2018/05/03 16:26:25 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		ft_fractal_pre_cl(t_mlx *mlx, float *param, int *tab_ite, int fract)
{
	cl_kernel	kernels[4];
	cl_mem		mem_param;
	cl_mem		mem_tab;
	size_t		size;
	int			ite_max;

	ite_max = 50;
	kernels[0] = OCL(k_man);
	kernels[1] = OCL(k_ju);
	kernels[2] = OCL(k_burn);
	kernels[3] = OCL(k_mult);
	size = (WIN_W_PRE) * (WIN_H_PRE);
	mem_param = clCreateBuffer(OCL(context), CL_MEM_READ_ONLY |
			CL_MEM_COPY_HOST_PTR, sizeof(float) * 9, param, NULL);
	mem_tab = clCreateBuffer(OCL(context), CL_MEM_WRITE_ONLY,
			sizeof(int) * size, NULL, NULL);
	clSetKernelArg(kernels[fract], 0, sizeof(cl_mem), &mem_param);
	clSetKernelArg(kernels[fract], 1, sizeof(cl_mem), &mem_tab);
	clSetKernelArg(kernels[fract], 2, sizeof(ite_max), &ite_max);
	clEnqueueNDRangeKernel(OCL(queue), kernels[fract], 1, NULL,
			&size, NULL, 0, NULL, NULL);
	clEnqueueReadBuffer(OCL(queue), mem_tab, CL_TRUE, 0,
			sizeof(int) * size, tab_ite, 0, NULL, NULL);
	clReleaseMemObject(mem_tab);
	clReleaseMemObject(mem_param);
}

void		ft_fractal_cl(t_mlx *mlx, float *param)
{
	cl_kernel	kernels[4];
	cl_mem		mem_param;
	cl_mem		mem_tab;
	size_t		size;

	kernels[0] = OCL(k_man);
	kernels[1] = OCL(k_ju);
	kernels[2] = OCL(k_burn);
	kernels[3] = OCL(k_mult);
	size = IMG_SIZE;
	mem_param = clCreateBuffer(OCL(context),
			CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
			sizeof(float) * 9, param, NULL);
	mem_tab = clCreateBuffer(OCL(context), CL_MEM_WRITE_ONLY,
			sizeof(int) * IMG_SIZE, NULL, NULL);
	clSetKernelArg(kernels[PARAM(fractal)], 0, sizeof(cl_mem), &mem_param);
	clSetKernelArg(kernels[PARAM(fractal)], 1, sizeof(cl_mem), &mem_tab);
	clSetKernelArg(kernels[PARAM(fractal)], 2, sizeof(PARAM(ite_max)),
			&PARAM(ite_max));
	clEnqueueNDRangeKernel(OCL(queue), kernels[PARAM(fractal)], 1, NULL,
			&size, NULL, 0, NULL, NULL);
	clEnqueueReadBuffer(OCL(queue), mem_tab, CL_TRUE, 0, sizeof(int)
			* (IMG_SIZE), FRACT(main_fract_ite), 0, NULL, NULL);
	clReleaseMemObject(mem_tab);
	clReleaseMemObject(mem_param);
}
