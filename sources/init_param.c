/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_param.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/22 16:37:51 by jbard             #+#    #+#             */
/*   Updated: 2018/05/04 16:21:20 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int			*ft_get_new_layer(t_mlx *mlx, int *old_layer)
{
	if (old_layer)
		free(old_layer);
	if (!(old_layer = (int *)malloc(sizeof(int) * IMG_SIZE)))
		ft_malloc_error(mlx);
	old_layer = (int *)ft_memset(old_layer, -1, sizeof(int) * IMG_SIZE);
	return (old_layer);
}

static void	ft_init_param_2(t_mlx *mlx)
{
	LAYER(layer_fract) = ft_get_new_layer(mlx, NULL);
	LAYER(layer_hud) = ft_get_new_layer(mlx, NULL);
	LAYER(layer_prev) = ft_get_new_layer(mlx, NULL);
	LAYER(layer_tmp) = NULL;
	FRACT(main_fract_ite) = ft_get_new_layer(mlx, NULL);
}

void		ft_init_param(t_mlx *mlx)
{
	PARAM(anim) = IS_OFF;
	PARAM(show_panels) = IS_ON;
	PARAM(right_panel_is_open) = IS_OFF;
	PARAM(right_handle_color) = MENU_HANDLE;
	PARAM(right_panel_fly_over_obj) = IS_OFF;
	PARAM(left_panel_is_open) = IS_OFF;
	PARAM(left_handle_color) = MENU_HANDLE;
	PARAM(left_panel_fly_over_obj) = IS_OFF;
	PARAM(effect) = 0;
	PARAM(color) = WHITE;
	PARAM(ite_max) = 50;
	PARAM(x) = WIN_W / 2;
	PARAM(y) = WIN_H / 2;
	PARAM(clean_high) = 0;
	PARAM(draw) = 0;
	PARAM(custom_color.color) = WHITE;
	PARAM(zoom) = 1;
	PARAM(pos_x) = 0;
	PARAM(pos_y) = 0;
	PARAM(c_r) = 0.285;
	PARAM(c_i) = 0.01;
	PARAM(n) = 2;
	PARAM(neg) = 1;
	ft_init_param_2(mlx);
}
