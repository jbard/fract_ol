/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_hud_2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 20:44:30 by jbard             #+#    #+#             */
/*   Updated: 2018/05/03 17:12:36 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_draw_high_line(int x, int y, int color, t_mlx *mlx)
{
	int		coor[4];

	coor[0] = x;
	coor[1] = y;
	coor[2] = x + PANEL_W_SIZE - 1;
	coor[3] = y;
	ft_coor_to_line(mlx, coor, color);
}

void		ft_draw_high(t_mlx *mlx, int x, int y, int color)
{
	int		i;

	i = -1;
	while (++i < PANEL_H_SIZE)
		ft_draw_high_line(x, y + i, color, mlx);
}

static void	select_left_panel(t_mlx *mlx)
{
	if (PARAM(effect) == 0)
		ft_draw_high(mlx, 0, 0, DARK_GREY);
	else if (PARAM(effect) == 1)
		ft_draw_high(mlx, 0, PANEL_H_SIZE, DARK_GREY);
	else if (PARAM(effect) == 2)
		ft_draw_high(mlx, 0, PANEL_H_SIZE * 2, DARK_GREY);
	else if (PARAM(effect) == 3)
		ft_draw_high(mlx, 0, PANEL_H_SIZE * 3, DARK_GREY);
	else
		ft_draw_high(mlx, 0, PANEL_H_SIZE * 4, DARK_GREY);
}

static void	select_right_panel(t_mlx *mlx)
{
	if (PARAM(fractal) == 0)
		ft_draw_high(mlx, WIN_W - PANEL_W_SIZE, 0, DARK_GREY);
	else if (PARAM(fractal) == 1)
		ft_draw_high(mlx, WIN_W - PANEL_W_SIZE, PANEL_H_SIZE, DARK_GREY);
	else if (PARAM(fractal) == 2)
		ft_draw_high(mlx, WIN_W - PANEL_W_SIZE, PANEL_H_SIZE * 2, DARK_GREY);
	else
		ft_draw_high(mlx, WIN_W - PANEL_W_SIZE, PANEL_H_SIZE * 3, DARK_GREY);
	ft_draw_high(mlx, WIN_W - PANEL_W_SIZE, PANEL_H_SIZE * 4, DARK_GREY);
	ft_draw_high(mlx, WIN_W - PANEL_W_SIZE, PANEL_H_SIZE * 5, DARK_GREY);
	ft_draw_high(mlx, WIN_W - PANEL_W_SIZE,
			PANEL_H_SIZE * 4 + 5, PARAM(custom_color.color));
}

void		ft_draw_selected(t_mlx *mlx)
{
	if (PARAM(left_panel_is_open))
		select_left_panel(mlx);
	if (PARAM(right_panel_is_open))
		select_right_panel(mlx);
}
