/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_hud_3.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 20:59:09 by jbard             #+#    #+#             */
/*   Updated: 2018/05/01 20:59:11 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_coor_cursor(t_mlx *mlx, int x, int y, int color)
{
	int		coor[4];

	coor[0] = x - 7;
	coor[1] = y;
	coor[2] = x - 1;
	coor[3] = y;
	ft_coor_to_line(mlx, coor, color);
}

static void	ft_draw_cursor(t_mlx *mlx, int x, int y, int color)
{
	int		i;

	i = -1;
	while (++i < 9)
		ft_coor_cursor(mlx, x, y + i, color);
}

static void	ft_coor_bar(t_mlx *mlx, int x, int y)
{
	int		coor[4];

	coor[0] = x;
	coor[1] = y;
	coor[2] = x + PANEL_W_SIZE - 1;
	coor[3] = y;
	ft_coor_to_line(mlx, coor, WHITE);
}

static void	ft_draw_bar(t_mlx *mlx, int x, int y)
{
	int		i;

	i = -1;
	while (++i < 3)
		ft_coor_bar(mlx, x, y + i);
}

void		ft_color_select(t_mlx *mlx)
{
	int		i;
	float	tmp;
	float	reg;
	int		color[3];

	color[0] = BLUE;
	color[1] = GREEN;
	color[2] = RED;
	i = -1;
	reg = (float)(PANEL_W_SIZE) / 255;
	while (++i < 3)
	{
		ft_draw_bar(mlx, WIN_W - PANEL_W_SIZE,
				(PANEL_H_SIZE) * 5 + PANEL_H_SIZE / 6 + i * 20);
		tmp = (float)(PARAM(custom_color.byte[i]) * reg);
		ft_draw_cursor(mlx, WIN_W - PANEL_W_SIZE + (int)tmp,
				(PANEL_H_SIZE) * 5 + (PANEL_H_SIZE / 6) +
				(i * 20) - 3, color[i]);
	}
}
