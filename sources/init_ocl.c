/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_ocl.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 16:27:34 by jbard             #+#    #+#             */
/*   Updated: 2018/05/03 16:27:35 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static cl_program	ft_create_program(t_mlx *mlx, cl_context context,
		cl_device_id device)
{
	cl_program	program;
	char		**sources;
	size_t		size[4];

	if (!(sources = (char **)ft_memalloc(sizeof(char *) * 4)))
		ft_malloc_error(mlx);
	sources[0] = ft_strdup(MAN_PATH);
	size[0] = ft_strlen(sources[0]);
	sources[1] = ft_strdup(JU_PATH);
	size[1] = ft_strlen(sources[1]);
	sources[2] = ft_strdup(BURN_PATH);
	size[2] = ft_strlen(sources[2]);
	sources[3] = ft_strdup(MULT_PATH);
	size[3] = ft_strlen(sources[3]);
	program = clCreateProgramWithSource(context, 4,
		(const char **)sources, size, NULL);
	clBuildProgram(program, 1, &device,
		"-Werror", NULL, NULL);
	free(sources[0]);
	free(sources[1]);
	free(sources[2]);
	free(sources[3]);
	free(sources);
	return (program);
}

void				ft_init_ocl(t_mlx *mlx)
{
	cl_platform_id			platform;
	cl_device_id			device;
	cl_context_properties	properties[3];
	cl_program				program;

	clGetPlatformIDs(1, &platform, NULL);
	clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &device, NULL);
	properties[0] = CL_CONTEXT_PLATFORM;
	properties[1] = (cl_context_properties)platform;
	properties[2] = 0;
	OCL(context) = clCreateContext(properties, 1, &device, NULL, NULL, NULL);
	OCL(queue) = clCreateCommandQueue(OCL(context), device,
				CL_QUEUE_PROFILING_ENABLE, NULL);
	program = ft_create_program(mlx, OCL(context), device);
	OCL(k_man) = clCreateKernel(program, "ft_mandelbrot", NULL);
	OCL(k_ju) = clCreateKernel(program, "ft_julia", NULL);
	OCL(k_burn) = clCreateKernel(program, "ft_burningship", NULL);
	OCL(k_mult) = clCreateKernel(program, "ft_multibrot", NULL);
}
