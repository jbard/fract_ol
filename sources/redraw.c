/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redraw.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/22 18:36:37 by jbard             #+#    #+#             */
/*   Updated: 2018/05/04 14:54:03 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_redraw_hud(t_mlx *mlx)
{
	LAYER(layer_hud) = ft_get_new_layer(mlx, LAYER(layer_hud));
	ft_draw_hud(mlx);
	ft_draw_all(mlx);
}

void	ft_redraw_main_fract(t_mlx *mlx)
{
	ft_main_fractal(mlx);
	ft_draw_all(mlx);
}

void	ft_redraw_left_menu(t_mlx *mlx)
{
	LAYER(layer_prev) = ft_get_new_layer(mlx, LAYER(layer_prev));
	ft_draw_pre(mlx);
	ft_draw_all(mlx);
}

void	ft_redraw_right_menu(t_mlx *mlx)
{
	LAYER(layer_prev) = ft_get_new_layer(mlx, LAYER(layer_prev));
	ft_draw_pre(mlx);
	ft_draw_all(mlx);
}
