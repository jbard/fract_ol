/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_hud.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 20:41:38 by jbard             #+#    #+#             */
/*   Updated: 2018/05/03 17:12:55 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_draw_arrow(int x, int dir, t_mlx *mlx)
{
	int		i;
	int		incr;
	int		coor[4];

	if (dir)
	{
		i = 0;
		incr = 1;
	}
	else
	{
		i = 3;
		incr = -1;
	}
	while (x++ && i > -1 && i < 4)
	{
		coor[0] = x;
		coor[1] = WIN_H / 2 - i;
		coor[2] = x;
		coor[3] = WIN_H / 2 + i;
		ft_coor_to_line(mlx, coor, WHITE);
		i += incr;
	}
}

static void	ft_draw_hud_line(int x, int color, t_mlx *mlx)
{
	int		coor[4];

	coor[0] = x;
	coor[1] = 0;
	coor[2] = x;
	coor[3] = WIN_H - 1;
	ft_coor_to_line(mlx, coor, color);
}

static void	ft_draw_panels(t_mlx *mlx)
{
	int		i;
	int		x;

	i = -1;
	while (++i < 10)
	{
		x = i + (PANEL_W_SIZE * PARAM(left_panel_is_open));
		ft_draw_hud_line(x, PARAM(left_handle_color), mlx);
		x = WIN_W - (i + 1) - (PANEL_W_SIZE * PARAM(right_panel_is_open));
		ft_draw_hud_line(x, PARAM(right_handle_color), mlx);
	}
	if (PARAM(left_panel_is_open) == IS_ON)
	{
		i = -1;
		while (++i < PANEL_W_SIZE)
			ft_draw_hud_line(i, MENU_DRAWER, mlx);
	}
	if (PARAM(right_panel_is_open) == IS_ON)
	{
		i = WIN_W;
		while (--i > WIN_W - WIN_W / 6 - 1)
			ft_draw_hud_line(i, MENU_DRAWER, mlx);
	}
}

void		ft_draw_hud(t_mlx *mlx)
{
	ft_draw_panels(mlx);
	ft_draw_arrow(2 + (PANEL_W_SIZE * PARAM(left_panel_is_open)),
		LEFT + PARAM(left_panel_is_open), mlx);
	ft_draw_arrow((WIN_W - 8) + ((WIN_W - PANEL_W_SIZE) *
		PARAM(right_panel_is_open)),
		RIGHT - PARAM(right_panel_is_open), mlx);
	ft_draw_selected(mlx);
	if (PARAM(right_panel_is_open))
		ft_color_select(mlx);
}
