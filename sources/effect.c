/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   effect.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 21:31:26 by jbard             #+#    #+#             */
/*   Updated: 2018/05/03 17:29:08 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

/*
** int *param = int param[5] {x, y, w_max, h_max, win_w, color, ite_max}
**	   						   0  1		 2	    3	   4	  5		   6
*/

void			ft_psycho(t_mlx *mlx, int *tab_ite, int *param)
{
	int		h;
	int		w;
	t_color color;
	t_color color_tmp;

	color.color = param[5];
	color_tmp.byte[3] = 0;
	h = -1;
	while (++h < param[3])
	{
		w = -1;
		while (++w < param[2])
		{
			if (tab_ite[h * param[2] + w] != param[6])
			{
				color_tmp.color = ((float)(param[6] - tab_ite[h
				* param[2] + w])) / (float)(param[6])
				* (color.color);
				LAYER(layer_tmp[(h + param[1]) * param[4] +
				w + param[0]]) = color_tmp.color;
			}
			else
				LAYER(layer_tmp[(h + param[1]) * param[4] + w + param[0]]) = 0;
		}
	}
}

static void		ft_btc_2(t_mlx *mlx, int *tab_ite, int *param, int h)
{
	int		w;
	int		i;
	t_color color;
	t_color color_tmp;

	color.color = param[5];
	color_tmp.byte[3] = 0;
	w = -1;
	while (++w < param[2])
	{
		if (tab_ite[h * param[2] + w] != param[6])
		{
			i = -1;
			while (++i < 3)
				color_tmp.byte[i] = color.byte[i] +
					((float)(param[6] - tab_ite[h *
						param[2] + w])) / (float)(param[6])
					* -color.byte[i];
			LAYER(layer_tmp[(h + param[1]) * param[4] +
					w + param[0]]) = color_tmp.color;
		}
		else
			LAYER(layer_tmp[(h + param[1]) * param[4] + w + param[0]]) = 0;
	}
}

void			ft_black_to_color(t_mlx *mlx, int *tab_ite, int *param)
{
	int		h;

	h = -1;
	while (++h < param[3])
		ft_btc_2(mlx, tab_ite, param, h);
}

void			ft_effect(t_mlx *mlx, int effect, int *tab_ite, int *param)
{
	void (*f_effect[5])(t_mlx*, int*, int*);

	f_effect[0] = &ft_color_to_black;
	f_effect[1] = &ft_black_to_color;
	f_effect[2] = &ft_psycho;
	f_effect[3] = &ft_comp;
	f_effect[4] = &ft_triad;
	f_effect[effect](mlx, tab_ite, param);
}
