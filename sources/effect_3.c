/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   effect_3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 17:14:48 by jbard             #+#    #+#             */
/*   Updated: 2018/05/03 17:27:21 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

/*
** int *param = int param[5] {x, y, w_max, h_max, win_w, color, ite_max}
**	   						   0  1		 2	    3	   4	  5		   6
*/

static t_color	*ft_triad_color(t_mlx *mlx, int color)
{
	t_color	tmp;
	t_color *ret;
	int		j;

	j = -1;
	tmp.color = color;
	if (!(ret = (t_color *)malloc(sizeof(t_color) * 3)))
		ft_malloc_error(mlx);
	while (++j < 3)
	{
		ret[0].byte[j] = tmp.byte[j % 3];
		ret[1].byte[j] = tmp.byte[(j + 1) % 3];
		ret[2].byte[j] = tmp.byte[(j + 2) % 3];
	}
	return (ret);
}

static void		ft_triad_2(t_mlx *mlx, int *tab_ite, int *param, int h)
{
	int		w;
	int		i;
	t_color *color;
	t_color color_tmp;

	color = ft_triad_color(mlx, param[5]);
	color_tmp.byte[3] = 0;
	w = -1;
	while (++w < param[2])
	{
		if (tab_ite[h * param[2] + w] != param[6])
		{
			i = -1;
			while (++i < 3)
				color_tmp.byte[i] = ((float)(param[6] - tab_ite[h *
							param[2] + w])) / (float)(param[6])
					* (color[tab_ite[h * param[2] + w] % 3].byte[i]);
			LAYER(layer_tmp[(h + param[1]) * param[4]
					+ w + param[0]]) = color_tmp.color;
		}
		else
			LAYER(layer_tmp[(h + param[1]) * param[4] + w + param[0]]) = 0;
	}
	free(color);
}

void			ft_triad(t_mlx *mlx, int *tab_ite, int *param)
{
	int		h;

	h = -1;
	while (++h < param[3])
		ft_triad_2(mlx, tab_ite, param, h);
}
