/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   effect_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 15:18:20 by jbard             #+#    #+#             */
/*   Updated: 2018/05/03 15:26:25 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

/*
** int *param = int param[5] {x, y, w_max, h_max, win_w, color, ite_max}
**							  0  1		2	   3	  4		 5		  6
** h_max pas necessaire pour le kernel
*/

/*
** Complementary color
**
** comp_byte = byte_max - (byte - byte_min)
*/

static t_color	ft_find_comp_color(t_color color)
{
	int		max;
	int		min;
	int		i;

	i = -1;
	max = 0;
	min = 255;
	while (++i < 3)
	{
		if (color.byte[i] > max)
			max = color.byte[i];
		if (color.byte[i] < min)
			min = color.byte[i];
	}
	color.byte[0] = max - (color.byte[0] - min);
	color.byte[1] = max - (color.byte[1] - min);
	color.byte[2] = max - (color.byte[2] - min);
	return (color);
}

void			ft_comp_2(t_mlx *mlx, int *tab_ite, int *param, int h)
{
	int		w;
	int		i[2];
	t_color color[2];
	t_color color_tmp;

	color[0].color = param[5];
	color[1] = ft_find_comp_color(color[0]);
	color_tmp.byte[3] = 0;
	w = -1;
	while (++w < param[2])
	{
		if (tab_ite[h * param[2] + w] != param[6])
		{
			i[0] = -1;
			i[1] = tab_ite[h * param[2] + w];
			while (++i[0] < 3)
				color_tmp.byte[i[0]] = ((float)(param[6] - i[1]))
					/ (float)(param[6])
					* (color[i[1] % 2].byte[i[0]]);
			LAYER(layer_tmp[(h + param[1]) * param[4]
					+ w + param[0]]) = color_tmp.color;
		}
		else
			LAYER(layer_tmp[(h + param[1]) * param[4] + w + param[0]]) = 0;
	}
}

void			ft_comp(t_mlx *mlx, int *tab_ite, int *param)
{
	int		h;

	h = -1;
	while (++h < param[3])
		ft_comp_2(mlx, tab_ite, param, h);
}
