/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_main_fractal.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 21:00:43 by jbard             #+#    #+#             */
/*   Updated: 2018/05/04 16:18:01 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static t_scale	ft_search_scale(t_mlx *mlx)
{
	if (PARAM(fractal) == 0)
		return (FRACT(mandelbrot));
	else if (PARAM(fractal) == 1)
		return (FRACT(julia));
	else if (PARAM(fractal) == 2)
		return (FRACT(burning));
	else
		return (FRACT(multibrot));
}

void			ft_main_fractal_ite_cl(t_mlx *mlx)
{
	float	param[9];
	t_scale scale;

	scale = ft_search_scale(mlx);
	param[0] = scale.x1 / PARAM(zoom) + PARAM(pos_x);
	param[1] = scale.x2 / PARAM(zoom) + PARAM(pos_x);
	param[2] = scale.y1 / PARAM(zoom) + PARAM(pos_y);
	param[3] = scale.y2 / PARAM(zoom) + PARAM(pos_y);
	param[4] = scale.scale * PARAM(zoom);
	param[5] = scale.weight;
	param[6] = PARAM(c_r) * PARAM(neg);
	param[7] = PARAM(c_i) * PARAM(neg);
	param[8] = PARAM(n);
	ft_fractal_cl(mlx, param);
}

void			ft_main_fractal_effect(t_mlx *mlx)
{
	int		param[7];

	LAYER(layer_tmp) = LAYER(layer_fract);
	param[0] = 0;
	param[1] = 0;
	param[2] = WIN_W;
	param[3] = WIN_H;
	param[4] = WIN_W;
	param[5] = PARAM(color);
	param[6] = PARAM(ite_max);
	ft_effect(mlx, PARAM(effect), FRACT(main_fract_ite), param);
}

void			ft_main_fractal(t_mlx *mlx)
{
	ft_main_fractal_ite_cl(mlx);
	ft_main_fractal_effect(mlx);
}
