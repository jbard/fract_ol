/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   effect_4.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 17:28:48 by jbard             #+#    #+#             */
/*   Updated: 2018/05/03 17:28:50 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

/*
** int *param = int param[5] {x, y, w_max, h_max, win_w, color, ite_max}
**	   						   0  1		 2	    3	   4	  5		   6
*/

static void		ft_ctb_2(t_mlx *mlx, int *tab_ite, int *param, int h)
{
	int		w;
	int		i;
	t_color color;
	t_color color_tmp;

	color.color = param[5];
	color_tmp.byte[3] = 0;
	w = -1;
	while (++w < param[2])
	{
		if (tab_ite[h * param[2] + w] != param[6])
		{
			i = -1;
			while (++i < 3)
				color_tmp.byte[i] = ((float)(param[6] - tab_ite[h *
					param[2] + w])) / (float)(param[6])
					* (color.byte[i]);
			LAYER(layer_tmp[(h + param[1]) * param[4] +
					w + param[0]]) = color_tmp.color;
		}
		else
			LAYER(layer_tmp[(h + param[1]) * param[4] + w + param[0]]) = 0;
	}
}

void			ft_color_to_black(t_mlx *mlx, int *tab_ite, int *param)
{
	int		h;

	h = -1;
	while (++h < param[3])
		ft_ctb_2(mlx, tab_ite, param, h);
}
