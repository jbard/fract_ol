/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_button.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 15:45:43 by jbard             #+#    #+#             */
/*   Updated: 2018/05/04 16:18:11 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_select_color(int x, int y, t_mlx *mlx)
{
	float	reg;

	reg = 255 / (float)(PANEL_W_SIZE);
	if (y >= PANEL_H_SIZE * 5 + PANEL_H_SIZE / 6 - 4
			&& y <= PANEL_H_SIZE * 5 + PANEL_H_SIZE / 6 + 4)
		PARAM(custom_color.byte[0]) =
			(x - (WIN_W - PANEL_W_SIZE)) * reg;
	else if (y >= PANEL_H_SIZE * 5 + PANEL_H_SIZE / 6 + 16
			&& y <= PANEL_H_SIZE * 5 + PANEL_H_SIZE / 6 + 24)
		PARAM(custom_color.byte[1]) =
			(x - (WIN_W - PANEL_W_SIZE)) * reg;
	else if (y >= PANEL_H_SIZE * 5 + PANEL_H_SIZE / 6 + 36
			&& y <= PANEL_H_SIZE * 5 + PANEL_H_SIZE / 6 + 54)
		PARAM(custom_color.byte[2]) =
			(x - (WIN_W - PANEL_W_SIZE)) * reg;
}

int			ft_button_loop(int button, int x, int y, t_mlx *mlx)
{
	if (x > WIN_W - 1 || x < 0
			|| y > WIN_H - 1 || y < 0)
		return (0);
	if (button == 1)
		ft_select_color(x, y, mlx);
	else if (button == SCROLL_DOWN && PARAM(zoom) > 1)
	{
		PARAM(zoom) /= 1.1;
		PARAM(pos_x) -= (PARAM(x) - WIN_W / 2) / (float)WIN_W / PARAM(zoom);
		PARAM(pos_y) -= (PARAM(y) - WIN_H / 2) / (float)WIN_H / PARAM(zoom);
		ft_main_fractal(mlx);
		ft_draw_all(mlx);
	}
	else if (button == SCROLL_UP && PARAM(zoom) < 35700)
	{
		PARAM(zoom) *= 1.1;
		PARAM(pos_x) += (PARAM(x) - WIN_W / 2) / (float)WIN_W / PARAM(zoom);
		PARAM(pos_y) += (PARAM(y) - WIN_H / 2) / (float)WIN_H / PARAM(zoom);
		ft_main_fractal(mlx);
		ft_draw_all(mlx);
	}
	return (0);
}
