/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_pre.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 16:23:17 by jbard             #+#    #+#             */
/*   Updated: 2018/05/04 14:54:57 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_choose_fract(int y, t_mlx *mlx)
{
	if (y < PANEL_H_SIZE)
		PARAM(fractal) = 0;
	else if (y < PANEL_H_SIZE * 2)
		PARAM(fractal) = 1;
	else if (y < PANEL_H_SIZE * 3)
		PARAM(fractal) = 2;
	else if (y < PANEL_H_SIZE * 4)
		PARAM(fractal) = 3;
	else if (y < PANEL_H_SIZE * 5 + 5)
		PARAM(color) = PARAM(custom_color.color);
	ft_main_fractal(mlx);
	ft_redraw_left_menu(mlx);
}

void	ft_choose_effect(int y, t_mlx *mlx)
{
	if (y < PANEL_H_SIZE)
		PARAM(effect) = 0;
	else if (y < PANEL_H_SIZE * 2)
		PARAM(effect) = 1;
	else if (y < PANEL_H_SIZE * 3)
		PARAM(effect) = 2;
	else if (y < PANEL_H_SIZE * 4)
		PARAM(effect) = 3;
	else if (y < PANEL_H_SIZE * 5)
		PARAM(effect) = 4;
	ft_main_fractal(mlx);
	ft_redraw_right_menu(mlx);
}
