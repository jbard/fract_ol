/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_pre.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 21:01:30 by jbard             #+#    #+#             */
/*   Updated: 2018/05/02 17:01:28 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_pre_fract_param(t_mlx *mlx, int x, int y, int *tab_ite)
{
	int		param[7];

	param[0] = x;
	param[1] = y;
	param[2] = WIN_W_PRE;
	param[3] = WIN_H_PRE;
	param[4] = WIN_W;
	param[5] = PARAM(color);
	param[6] = 50;
	ft_effect(mlx, PARAM(effect), tab_ite, param);
}

static void	ft_pre_fract_cust(t_mlx *mlx, int x, int y, int *tab_ite)
{
	int		param[7];

	param[0] = x;
	param[1] = y;
	param[2] = WIN_W_PRE;
	param[3] = WIN_H_PRE;
	param[4] = WIN_W;
	param[5] = PARAM(custom_color.color);
	param[6] = 50;
	ft_effect(mlx, PARAM(effect), tab_ite, param);
}

static int	*ft_get_fract_tab(t_mlx *mlx)
{
	if (PARAM(fractal) == 0)
		return (FRACT(preview_man_ite));
	else if (PARAM(fractal) == 1)
		return (FRACT(preview_ju_ite));
	else if (PARAM(fractal) == 2)
		return (FRACT(preview_burn_ite));
	else
		return (FRACT(preview_mult_ite));
}

void		ft_fractal_menu(t_mlx *mlx)
{
	int		*tab_tmp;

	LAYER(layer_tmp) = LAYER(layer_prev);
	ft_pre_fract_param(mlx, WIN_W - WIN_W / 6 + 5,
			5, FRACT(preview_man_ite));
	ft_pre_fract_param(mlx, WIN_W - WIN_W / 6 + 5,
			PANEL_H_SIZE + 5, FRACT(preview_ju_ite));
	ft_pre_fract_param(mlx, WIN_W - WIN_W / 6 + 5,
			PANEL_H_SIZE * 2 + 5, FRACT(preview_burn_ite));
	ft_pre_fract_param(mlx, WIN_W - WIN_W / 6 + 5,
			PANEL_H_SIZE * 3 + 5, FRACT(preview_mult_ite));
	tab_tmp = ft_get_fract_tab(mlx);
	ft_pre_fract_cust(mlx, WIN_W - WIN_W / 6 + 5,
			PANEL_H_SIZE * 4 + 10, tab_tmp);
}

void		ft_draw_pre(t_mlx *mlx)
{
	ft_left_pre(mlx);
	ft_fractal_menu(mlx);
}
