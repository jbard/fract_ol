/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_key.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 15:46:48 by jbard             #+#    #+#             */
/*   Updated: 2018/05/07 14:05:10 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_key_2(t_mlx *mlx, int key)
{
	if (key == KEY_R)
	{
		PARAM(zoom) = 1;
		PARAM(pos_x) = 0;
		PARAM(pos_y) = 0;
		ft_redraw_main_fract(mlx);
	}
	else if (PARAM(fractal) == 3 && key == KEY_PAD_ADD)
	{
		PARAM(n++);
		ft_redraw_main_fract(mlx);
	}
	else if (PARAM(fractal) == 3 && key == KEY_PAD_SUB
			&& PARAM(n) != 2)
	{
		PARAM(n--);
		ft_redraw_main_fract(mlx);
	}
	else if (key == KEY_N)
	{
		PARAM(neg) *= -1;
		ft_redraw_main_fract(mlx);
	}
}

int			ft_key(int key, t_mlx *mlx)
{
	if (key == KEY_ESCAPE)
		ft_exit_properly(mlx);
	if (key == KEY_H)
	{
		if (PARAM(show_panels))
			PARAM(show_panels) = IS_OFF;
		else
			PARAM(show_panels) = IS_ON;
		ft_draw_all(mlx);
	}
	if (key == KEY_S)
	{
		if (PARAM(anim))
			PARAM(anim) = IS_OFF;
		else
			PARAM(anim) = IS_ON;
	}
	else
		ft_key_2(mlx, key);
	return (0);
}

static void	ft_other_key_loop(t_mlx *mlx, int key)
{
	if (key == KEY_LEFT)
	{
		PARAM(pos_x) -= 0.3 / PARAM(zoom);
		ft_redraw_main_fract(mlx);
	}
	else if (key == KEY_RIGHT)
	{
		PARAM(pos_x) += 0.3 / PARAM(zoom);
		ft_redraw_main_fract(mlx);
	}
	else if (key == KEY_UP)
	{
		PARAM(pos_y) -= 0.3 / PARAM(zoom);
		ft_redraw_main_fract(mlx);
	}
	else if (key == KEY_DOWN)
	{
		PARAM(pos_y) += 0.3 / PARAM(zoom);
		ft_redraw_main_fract(mlx);
	}
}

int			ft_key_loop(int key, t_mlx *mlx)
{
	if (key == KEY_EQUAL)
	{
		PARAM(ite_max) += 10;
		ft_putnbr(PARAM(ite_max));
		ft_putendl("");
		ft_redraw_main_fract(mlx);
	}
	else if (key == KEY_MINUS)
	{
		PARAM(ite_max) -= 10;
		ft_putnbr(PARAM(ite_max));
		ft_putendl("");
		ft_redraw_main_fract(mlx);
	}
	else
		ft_other_key_loop(mlx, key);
	return (0);
}
