/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_var.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 16:02:48 by jbard             #+#    #+#             */
/*   Updated: 2018/05/03 16:29:40 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static t_scale	ft_scale_fract(t_scale fract, int height, int weight)
{
	fract.scale = 1;
	fract.height = height;
	fract.weight = weight;
	while ((fract.x2 - fract.x1) * fract.scale < weight
			&& (fract.y2 - fract.y1) * fract.scale < height)
		fract.scale++;
	while ((fract.x2 - fract.x1) * fract.scale < weight)
	{
		fract.x1 -= 0.01;
		fract.x2 += 0.01;
	}
	while ((fract.y2 - fract.y1) * fract.scale < height)
	{
		fract.y1 -= 0.01;
		fract.y2 += 0.01;
	}
	return (fract);
}

static float	*ft_set_param(t_scale scale, t_mlx *mlx)
{
	float	*param;

	if (!(param = (float *)malloc(sizeof(float) * 9)))
		ft_malloc_error(mlx);
	param[0] = scale.x1;
	param[1] = scale.x2;
	param[2] = scale.y1;
	param[3] = scale.y2;
	param[4] = scale.scale;
	param[5] = scale.weight;
	param[6] = 0.285;
	param[7] = 0.01;
	param[8] = 2;
	return (param);
}

static int		*ft_init_preview_ite(t_mlx *mlx, t_scale scale, int fract)
{
	float	*param;
	int		*tab_ite;

	tab_ite = NULL;
	if (!(tab_ite = (int *)malloc(sizeof(int) * (WIN_H_PRE) * (WIN_W_PRE))))
		ft_malloc_error(mlx);
	param = ft_set_param(scale, mlx);
	ft_fractal_pre_cl(mlx, param, tab_ite, fract);
	free(param);
	return (tab_ite);
}

static t_scale	ft_set_border(float x1, float x2, float y1, float y2)
{
	t_scale		scale;

	scale.x1 = x1;
	scale.x2 = x2;
	scale.y1 = y1;
	scale.y2 = y2;
	return (scale);
}

void			ft_init_var(t_mlx *mlx)
{
	t_scale		scale;

	scale = ft_set_border(-2.1, 0.6, -1.2, 1.2);
	FRACT(mandelbrot) = ft_scale_fract(scale, WIN_H, WIN_W);
	scale = ft_scale_fract(scale, WIN_H_PRE, WIN_W_PRE);
	FRACT(preview_man_ite) = ft_init_preview_ite(mlx, scale, 0);
	scale = ft_set_border(-1, 1, -1.2, 1.2);
	FRACT(julia) = ft_scale_fract(scale, WIN_H, WIN_W);
	scale = ft_scale_fract(scale, WIN_H_PRE, WIN_W_PRE);
	FRACT(preview_ju_ite) = ft_init_preview_ite(mlx, scale, 1);
	scale = ft_set_border(-2.1, 1, -1.8, 1.0);
	FRACT(burning) = ft_scale_fract(scale, WIN_H, WIN_W);
	scale = ft_scale_fract(scale, WIN_H_PRE, WIN_W_PRE);
	FRACT(preview_burn_ite) = ft_init_preview_ite(mlx, scale, 2);
	scale = ft_set_border(-2.1, 0.6, -1.2, 1.2);
	FRACT(multibrot) = ft_scale_fract(scale, WIN_H, WIN_W);
	scale = ft_scale_fract(scale, WIN_H_PRE, WIN_W_PRE);
	FRACT(preview_mult_ite) = ft_init_preview_ite(mlx, scale, 3);
	ft_init_param(mlx);
}
