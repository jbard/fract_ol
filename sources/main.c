/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/26 16:22:35 by jbard             #+#    #+#             */
/*   Updated: 2018/05/04 16:58:44 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_free_all(t_mlx *mlx)
{
	if (mlx)
	{
		if (MLX(mlx_ptr))
		{
			mlx_destroy_image(mlx->mlx_ptr, IMG(img_ptr));
			mlx_destroy_window(mlx->mlx_ptr, mlx->win);
		}
		if (FRACT(main_fract_ite))
			free(FRACT(main_fract_ite));
		if (FRACT(preview_man_ite))
			free(FRACT(preview_man_ite));
		if (FRACT(preview_ju_ite))
			free(FRACT(preview_ju_ite));
		if (FRACT(preview_burn_ite))
			free(FRACT(preview_burn_ite));
		if (FRACT(preview_mult_ite))
			free(FRACT(preview_mult_ite));
		if (LAYER(layer_fract))
			free(LAYER(layer_fract));
		if (LAYER(layer_hud))
			free(LAYER(layer_hud));
		if (LAYER(layer_prev))
			free(LAYER(layer_prev));
		free(mlx);
	}
}

void		ft_exit_properly(t_mlx *mlx)
{
	ft_free_all(mlx);
	exit(0);
}

void		ft_malloc_error(t_mlx *mlx)
{
	ft_putendl("error: memory allocation fail");
	ft_exit_properly(mlx);
}

void		ft_check_flag(t_mlx *mlx, char *str)
{
	if (!ft_strcmp(str, "-m"))
		PARAM(fractal) = 0;
	else if (!ft_strcmp(str, "-j"))
		PARAM(fractal) = 1;
	else if (!ft_strcmp(str, "-b"))
		PARAM(fractal) = 2;
	else if (!ft_strcmp(str, "-mu"))
		PARAM(fractal) = 3;
	else
	{
		ft_putendl("usage: ./fractol [-m | -j | -b | -mu]");
		ft_exit_properly(mlx);
	}
}

int			main(int argc, char **argv)
{
	t_mlx	*mlx;

	if (argc != 2)
	{
		ft_putendl("usage: ./fractol [-m | -j | -b | -mu]");
		exit(0);
	}
	if (!(mlx = (t_mlx *)ft_memalloc(sizeof(t_mlx))))
		ft_malloc_error(mlx);
	ft_check_flag(mlx, argv[1]);
	mlx->mlx_ptr = mlx_init();
	mlx->win = mlx_new_window(mlx->mlx_ptr, WIN_W, WIN_H, "Fractol");
	IMG(img_ptr) = mlx_new_image(mlx->mlx_ptr, WIN_W, WIN_H);
	IMG(data) = (int *)mlx_get_data_addr(IMG(img_ptr),
			&IMG(bpp), &IMG(size_l), &IMG(endian));
	ft_init_ocl(mlx);
	ft_init_var(mlx);
	ft_init_event(mlx);
	return (0);
}
