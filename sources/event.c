/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 15:29:47 by jbard             #+#    #+#             */
/*   Updated: 2018/05/03 15:44:15 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_clean_high(t_mlx *mlx)
{
	PARAM(right_handle_color) = MENU_HANDLE;
	PARAM(left_handle_color) = MENU_HANDLE;
	PARAM(clean_high) = 0;
	ft_draw_hud(mlx);
	ft_draw_all(mlx);
}

int			ft_mouse_pos(int x, int y, t_mlx *mlx)
{
	if (x < 0 || x > WIN_W || y < 0 || y > WIN_H)
		return (0);
	PARAM(x) = x;
	PARAM(y) = y;
	if (PARAM(fractal) == 1 && PARAM(anim))
	{
		PARAM(c_r) = x / (float)WIN_W;
		PARAM(c_i) = y / (float)WIN_H;
		ft_main_fractal(mlx);
		PARAM(draw) = 1;
	}
	if (PARAM(show_panels)
			&& (x < 11 + (PANEL_W_SIZE * PARAM(left_panel_is_open))
			|| x > WIN_W - 10 - (PANEL_W_SIZE * PARAM(right_panel_is_open))))
		ft_draw_event_hud(mlx);
	else if (PARAM(clean_high))
		ft_clean_high(mlx);
	if (PARAM(draw))
	{
		ft_draw_all(mlx);
		PARAM(draw) = 0;
	}
	return (0);
}

static void	ft_left_option(t_mlx *mlx, int x, int y)
{
	int		left_tmp;

	left_tmp = (PANEL_W_SIZE * PARAM(left_panel_is_open));
	if (x > left_tmp)
	{
		if (PARAM(left_panel_is_open))
			PARAM(left_panel_is_open) = IS_OFF;
		else
			PARAM(left_panel_is_open) = IS_ON;
		ft_redraw_hud(mlx);
	}
	else
		ft_choose_effect(y, mlx);
}

int			ft_button(int button, int x, int y, t_mlx *mlx)
{
	int		right_tmp;

	if (button != 1 || !PARAM(show_panels)
			|| (x > 10 + PANEL_W_SIZE * PARAM(left_panel_is_open)
				&& x < WIN_W - 9 - PANEL_W_SIZE * PARAM(right_panel_is_open)))
		return (0);
	right_tmp = (PANEL_W_SIZE * PARAM(right_panel_is_open));
	if (x > WIN_W - right_tmp - 10)
	{
		if (x < WIN_W - right_tmp)
		{
			if (PARAM(right_panel_is_open))
				PARAM(right_panel_is_open) = IS_OFF;
			else
				PARAM(right_panel_is_open) = IS_ON;
			ft_redraw_hud(mlx);
		}
		else
			ft_choose_fract(y, mlx);
	}
	else
		ft_left_option(mlx, x, y);
	return (0);
}

void		ft_init_event(t_mlx *mlx)
{
	ft_draw_first(mlx);
	mlx_key_hook(mlx->win, ft_key, (void*)mlx);
	mlx_hook(mlx->win, KEYPRESS, KEYPRESSMASK, ft_key_loop, mlx);
	mlx_hook(mlx->win, MOTIONNOTIFY, ENTERWINDOWMASK, ft_mouse_pos, mlx);
	mlx_hook(mlx->win, BUTTONRELEASE, BUTTONRELEASEMASK, ft_button, mlx);
	mlx_hook(mlx->win, BUTTONPRESS, BUTTONPRESSMASK, ft_button_loop, mlx);
	mlx_loop(mlx);
}
