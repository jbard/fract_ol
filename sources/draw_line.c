/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 21:00:03 by jbard             #+#    #+#             */
/*   Updated: 2018/05/02 17:53:15 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_swap_coor(t_line *line)
{
	int	tmp;

	if (line->x1 < line->x2)
	{
		tmp = line->x1;
		line->x1 = line->x2;
		line->x2 = tmp;
		tmp = line->y1;
		line->y1 = line->y2;
		line->y2 = tmp;
	}
}

static void	ft_draw_line(t_line *line)
{
	int ex;
	int ey;
	int err;
	int e;

	ft_swap_coor(line);
	ex = abs(line->x2 - line->x1);
	ey = abs(line->y2 - line->y1);
	err = (ex > ey ? ex : -ey) / 2;
	while (line->x1 != line->x2 || line->y1 != line->y2)
	{
		e = err;
		line->LAYER(layer_hud)[line->y1 * WIN_W + line->x1] = line->color;
		if (e > -ex)
		{
			line->x1--;
			err -= ey;
		}
		if (e < ey)
		{
			line->y1 > line->y2 ? line->y1-- : line->y1++;
			err += ex;
		}
	}
	line->LAYER(layer_hud)[line->y1 * WIN_W + line->x1] = line->color;
}

void		ft_coor_to_line(t_mlx *mlx, int *coor, int color)
{
	t_line	line;

	line.x1 = coor[0];
	line.y1 = coor[1];
	line.x2 = coor[2];
	line.y2 = coor[3];
	line.color = color;
	line.mlx = mlx;
	ft_draw_line(&line);
}
