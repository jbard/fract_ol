/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 16:00:01 by jbard             #+#    #+#             */
/*   Updated: 2018/05/04 15:59:44 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __FRACTOL_H
# define __FRACTOL_H

/*
** INCLUDES
*/

# include "mlx.h"
# include "get_next_line.h"
# include "config.h"

# define CL_USE_DEPRECATED_OPENCL_1_2_APIS

# ifdef __APPLE__
#  include <OpenCL/cl.h>
# else
#  include <CL/cl.h>
# endif

/*
** DEFINE PATH
*/

# define MLX(q)		mlx->q
# define IMG(q)		MLX(img.q)
# define PARAM(q)	IMG(param.q)
# define FRACT(q)	IMG(fract.q)
# define LAYER(q)	IMG(layer.q)
# define OCL(q)		IMG(ocl.q)

/*
** DEFINE KEY
*/

# define KEY_S			1
# define KEY_D			2
# define KEY_H			4
# define KEY_R			15
# define KEY_EQUAL		24
# define KEY_MINUS		27
# define KEY_L			37
# define KEY_ESCAPE		53
# define KEY_PAD_ADD	69
# define KEY_PAD_SUB	78
# define KEY_N			45

# define KEY_LEFT 		123
# define KEY_RIGHT 		124
# define KEY_DOWN 		125
# define KEY_UP 		126

# define SCROLL_DOWN	4
# define SCROLL_UP		5

/*
** EVENT
*/

# define KEYPRESSMASK		(1L<<0)
# define KEYRELEASEMASK		(1L<<1)
# define ENTERWINDOWMASK	(1L<<4)
# define BUTTONPRESSMASK	(1L<<2)
# define BUTTONRELEASEMASK	(1L<<3)
# define LEAVEWINDOWMASK	(1L<<5)

# define KEYPRESS		2
# define KEYRELEASE		3
# define BUTTONPRESS	4
# define BUTTONRELEASE	5
# define MOTIONNOTIFY	6

/*
** COLORS
*/

# define MENU_HANDLE			0x282622
# define MENU_HANDLE_H			0x222428
# define MENU_PREVIEW_SELECT	0x282622
# define MENU_PREVIEW_H 		0xFFFFFF
# define MENU_DRAWER			0x4a4c50

# define DARK_GREY		0x282622
# define LIGHT_GREY		0x222428

# define WHITE			0xFFFFFF
# define RED			0xFF0000
# define GREEN			0x00FF00
# define BLUE			0x0000FF

/*
** KERNEL PATH
*/

# define MAN_PATH		"./kernels/mandelbrot.cl"
# define JU_PATH		"./kernels/julia.cl"
# define BURN_PATH		"./kernels/burningship.cl"
# define MULT_PATH		"./kernels/multibrot.cl"

/*
** DEFINE MISCELLANEOUS
*/

# define IS_ON			1
# define IS_OFF			0

# define LEFT			0
# define RIGHT			1

/*
** MAX 2560 * 1395
** MIN 100 * 100
*/

# if USER_WIDTH < 500
#  define WIN_W	500
# elif USER_WIDTH > 2560
#  define WIN_W	2560
# else
#  define WIN_W	USER_WIDTH
# endif

# if USER_HEIGHT < 720
#  define WIN_H	720
# elif USER_HEIGHT > 1395
#  define WIN_H	1395
# else
#  define WIN_H	USER_HEIGHT
# endif

# define IMG_SIZE		WIN_W * WIN_H
# define PANEL_W_SIZE	WIN_W / 6
# define PANEL_H_SIZE	WIN_H / 6
# define WIN_W_PRE		WIN_W / 6 - 10
# define WIN_H_PRE		WIN_H / 6 - 10

# define ITE_PREVIEW 50

/*
** UNION
*/

typedef union	u_color
{
	int				color;
	unsigned char	byte[4];
}				t_color;

/*
** STRUCT
*/

typedef struct	s_param
{
	int				right_panel_is_open;
	int				right_handle_color;
	int				right_panel_fly_over_obj;

	int				left_panel_is_open;
	int				left_handle_color;
	int				left_panel_fly_over_obj;

	int				anim;
	int				show_panels;
	int				fractal;
	int				effect;
	int				color;
	int				ite_max;
	int				x;
	int				y;
	int				clean_high;
	int				draw;

	float			zoom;
	float			pos_x;
	float			pos_y;
	float			c_i;
	float			c_r;
	float			n;
	float			neg;
	t_color			custom_color;
}				t_param;

typedef struct	s_scale
{
	float			x1;
	float			x2;
	float			y1;
	float			y2;
	float			scale;
	float			height;
	float			weight;
}				t_scale;

typedef	struct	s_fract
{
	t_scale			mandelbrot;
	t_scale			julia;
	t_scale			burning;
	t_scale			multibrot;

	int				*main_fract_ite;
	int				*preview_man_ite;
	int				*preview_ju_ite;
	int				*preview_burn_ite;
	int				*preview_mult_ite;
}				t_fract;

typedef struct	s_layer
{
	int				*layer_fract;
	int				*layer_tmp;
	int				*layer_hud;
	int				*layer_prev;
}				t_layer;

typedef struct	s_ocl
{
	cl_context			context;
	cl_command_queue	queue;
	cl_kernel			k_man;
	cl_kernel			k_ju;
	cl_kernel			k_burn;
	cl_kernel			k_mult;
}				t_ocl;

typedef	struct	s_img
{
	void			*img_ptr;
	int				*data;
	int				size_l;
	int				bpp;
	int				endian;

	t_layer			layer;
	t_fract			fract;
	t_param			param;
	t_ocl			ocl;
}				t_img;

typedef struct	s_mlx
{
	void			*mlx_ptr;
	void			*win;
	t_img			img;
}				t_mlx;

typedef struct	s_line
{
	int				x1;
	int				x2;
	int				y1;
	int				y2;
	int				color;
	t_mlx			*mlx;
}				t_line;

/*
** PROTOTYPE
*/

void			ft_malloc_error(t_mlx *mlx);
void			ft_exit_properly(t_mlx *mlx);

void			ft_coor_to_line(t_mlx *mlx, int *coor, int color);
void			ft_draw_all(t_mlx *mlx);
void			ft_draw_first(t_mlx *mlx);

void			ft_init_event(t_mlx *mlx);
int				ft_key(int key, t_mlx *mlx);
int				ft_mouse_pos(int x, int y, t_mlx *mlx);
int				ft_button(int button, int x, int y, t_mlx *mlx);
int				ft_key_loop(int key, t_mlx *mlx);
int				ft_button_loop(int button, int x, int y, t_mlx *mlx);

void			ft_init_var(t_mlx *mlx);

void			ft_init_param(t_mlx *mlx);
int				*ft_get_new_layer(t_mlx *mlx, int *old_layer);

void			ft_mandelbrot(float *param, int *tab_ite, int iteration_max);
void			ft_julia(float *param, int *tab_ite, int iteration_max);
void			ft_fractal(t_mlx *mlx, float *param);

void			ft_main_fractal(t_mlx *mlx);
void			ft_main_fractal_ite(t_mlx *mlx);
void			ft_main_fractal_effect(t_mlx *mlx);

void			ft_draw_pre(t_mlx *mlx);
void			ft_fractal_menu(t_mlx *mlx);

void			ft_color_menu(t_mlx *mlx);
void			ft_effect_menu(t_mlx *mlx);
void			ft_left_pre(t_mlx *mlx);

void			ft_draw_event_hud(t_mlx *mlx);

void			ft_draw_palette_menu(t_mlx *mlx);
void			ft_draw_hud(t_mlx *mlx);

void			ft_draw_high(t_mlx *mlx, int x, int y, int color);
void			ft_draw_selected(t_mlx *mlx);

void			ft_redraw_hud(t_mlx *mlx);
void			ft_redraw_fract(t_mlx *mlx);
void			ft_redraw_main_fract(t_mlx *mlx);
void			ft_redraw_left_menu(t_mlx *mlx);
void			ft_redraw_right_menu(t_mlx *mlx);

void			ft_effect(t_mlx *mlx, int effect, int *tab_ite, int *param);
void			ft_black_to_color(t_mlx *mlx, int *tab_ite, int *param);
void			ft_color_to_black(t_mlx *mlx, int *tab_ite, int *param);
void			ft_psycho(t_mlx *mlx, int *tab_ite, int *param);

void			ft_triad(t_mlx *mlx, int *tab_ite, int *param);

void			ft_choose_fract(int y, t_mlx *mlx);
void			ft_choose_color(int y, t_mlx *mlx);
void			ft_choose_effect(int y, t_mlx *mlx);

void			ft_color_select(t_mlx *mlx);

void			ft_init_ocl(t_mlx *mlx);

void			ft_fractal_cl(t_mlx *mlx, float *param);
void			ft_fractal_pre_cl(t_mlx *mlx, float *param,
				int *tab_ite, int fract);

void			ft_comp(t_mlx *mlx, int *tab_ite, int *param);

#endif
