#include "./kernels/kernel.h"

__kernel void	ft_mandelbrot(
	const __constant float *param,
	__global int *tab_ite,
	const __private int iteration_max)
{
	__private float	c[2];
	__private float	z[2];
	__private float	tmp;
	__private int	i;

	c[R] = (get_global_id(0) % (int)param[WEIGHT]) / param[SCALE] + param[X1];
	c[I] = (get_global_id(0) / (int)param[WEIGHT]) / param[SCALE] + param[Y1];
	z[R] = 0;
	z[I] = 0;

	i = 0;
	while (z[R] * z[R] + z[I] * z[I] <= 4 && i < iteration_max)
	{
		tmp = z[R];
		z[R] = z[R] * z[R] - z[I] * z[I] + c[R];
		z[I] = 2 * z[I] * tmp + c[I];
		i++;
	}
	tab_ite[get_global_id(0)] = i;
}
