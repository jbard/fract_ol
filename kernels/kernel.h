/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   kernel.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/04 16:27:42 by jbard             #+#    #+#             */
/*   Updated: 2018/05/04 16:27:44 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __KERNEL_H
# define __KERNEL_H

# define X1 0
# define X2 1
# define Y1 2
# define Y2 3
# define SCALE 4
# define WEIGHT 5
# define C_R 6
# define C_I 7

# define R 0
# define I 1

#endif
